#!/bin/bash -ex

# CONFIG
prefix="AudacityLame"
suffix=""
munki_package_name="AudacityLameLibrary"
display_name="Audacity: LAME encoding library"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o lame* -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# unzip
unzip lame*

# make DMG from the inner prefpane
mkdir app_tmp
mv *pkg app_tmp
hdiutil create -srcfolder app_tmp -format UDZO -o app.dmg

# unzip
unzip lame*

# rename pkg to general name
mv *pkg lame.pkg

## Unpack
/usr/sbin/pkgutil --expand "`pwd`/lame.pkg" pkg

mkdir build-root

# Unpack Payload within build-root
(cd build-root; pax -rz -f ../pkg/root.pkg/Payload)

mkdir tmp

# Move contents of build-root to a tmp directory
(cd build-root; mv * ../tmp)

# Create path to files
mkdir build-root/usr
mkdir build-root/usr/local

# Move contents of Payload out of temp to the path specified in build-root
(cd tmp; mv * ../build-root/usr/local)

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root/usr/local/lib -name '*.app' -or -name '*.dylib' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
